# Árbol Balanceado - AVL con ID de proteínas

El programa crea un árbol balanceado, o AVL por sus creadores, dos matematicos rusos Adelson-Velskii y Landis, insertando todos los ID dentro del archivo txt "pdbs.txt". Al ser un arbol balanceado los inserta de manera ordenada y con ID's sin repetir. De manera posterior el usuario puede manipular el árbol mediante acciones como insertar un ID, eliminar un ID, modificar un ID (eliminar uno viejo e insertar uno nuevo), buscar un ID y crear/visualizar el gráfico del árbol.

## Comenzando

Lo primero que el programa realiza el la creación del árbol, la raíz a manipular de este e insertar los pdbs desde el archivo. Luego procede a preguntar al usuario lo que desea hacer. Insertar un ID, Eliminar un ID, Buscar un ID, Modificar un ID, crear un gráfico con el árbol para poder visualizarlo y finalmente salir del programa.

Todos los archivos pdb están dentro de un solo txt, "pdbs.txt".

## Prerrequisitos

### Sistema Operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

### Make

Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:

`sudo apt install make`

**En caso de no usar make**

Se puede ejecutar el programa sin make (revisar **Ejecutando las pruebas**).

## Ejecutando las pruebas

### Iniciar programa "compilar"

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:

`make`

**Sin make**

En la terminar situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:

`g++ Programa.cpp Arbol.cpp Grafo.cpp -o programa`

### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando:

`./programa`

El programa comienza generando un árbol, una raíz e ingresando los pdbs disponibles en el archivo "pdbs.txt", leyendo linea por línea de ese archivo específico e insertándolos de manera ordenada. Posterior a esto el programa procede a preguntarle al usuario lo que desea hacer para manipular este árbol solicitando una de las siguientes opciones:


**Opción 1, Insertar un ID:** Al insertar el número 1 procede a preguntar cual será el ID que desee agregar. El programa realiza un proceso de balanceo debido a que los nodos deben tener ciertos factores de equilibrio, así que al agregar dependiendo de si es menor o mayor a los nodos presentes ya en el árbol realiza sus rotaciones específicas, si es que las necesita y lo agrega. Esto siempre y cuando el ID que se desea agregar no esté dentro del árbol ya, no se pueden repetir.

**Opción 2, Buscar:** Al insertar el número 2 procede a preguntar cual es el nodo que quiere buscar. El programa señala si encontró el nodo o no.

**Opción 3, Eliminar:** Al insertar el número 3 procede a preguntar cual es el ID que desea eliminar. El programa busca este nodo y siempre cuando exista lo elimina. El eliminar un nodo puede alterar los factores de equilibrio de otros nodos por lo que realiza reestructuraciones en el nodo dependiendo del nodo eliminado.

**Opción 4, Modificar:** Al insertar el número 4 se procede a preguntar cual es el ID que desea eliminar. Si este nodo existe dentro del árbol entonces procede a preguntar por cual lo quiere reemplazar, si por el que quiere reemplazar no se repite entonces se elimina el nodo señalado y luego se reemplaza. Si el nodo que desea eliminar no existe y por el que desea reemplazar existe entonces no se modifica nada. Esta eliminación e inserción se realizan con sus conjuntas validaciones y reestructuraciones para mantener su balance.

**Opción 5, Generar grafo:** Al insertar el número 5 el programa crea un archivo .txt que es la base para crear un archivo .png con el gráfico del árbol, procediendo a abrir esta imagen y asi se visualiza el árbol como está en ese momento.

**Opción 0, Salir:** Al insertar el número 0 el programa se cierra.

Posterior a cualquier opción ingresada, que no sea 0, el programa volverá a este menú a hacer las mismas preguntas hasta que se seleccione salir del programa.

La salida del programa es con la opción 0 o con cualquier otra, entero o no, que no sea desde 1 a 5. Si desea salir del programa en cualquier momento debe presionar los botones `ctl+c`.


## Despliegue

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con
- Lenguaje c++: librería iostream, fstream, unistd.h

## Autor
- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl
