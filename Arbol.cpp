#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
using namespace std;

// nodo
#include "programa.h"  // raiz del arbol

//grafo
#include "Grafo.h"

//arbol
#include "Arbol.h"

#define TRUE 1
#define FALSE 0

// constructor
Arbol::Arbol(){

}

// inserta un elemento de manera balanceada
void Arbol::InsercionBalanceado(Nodo *&nodo_rev, int *BO, string infor) {
  // nodos temporales
  Nodo *nodo = NULL;
  Nodo *nodo1 = NULL;
  Nodo *nodo2 = NULL;
	//temporal
	nodo = nodo_rev;

  // cuando el nodo no está vacío
  if (nodo != NULL) {
    // si el nodo a agregar es menor que el en revisión retorna un negativo
    if (infor.compare(nodo->info) <= -1) {
      // se ingresa por el lado izquierdo
      InsercionBalanceado(nodo->izq, BO, infor);
      // si el arbol ha crecido
      if(*BO == TRUE) {
        // depende de su factor de equlibrio el balanceo que tendrá la inserción
        switch (nodo->FE) {
          // si el FE es 1
          case 1:
          // ahora será 0
          nodo->FE = 0;
          *BO = FALSE;
          break;
          // si el FE es 0
          case 0:
          // ahora será -1
          nodo->FE = -1;
          break;

          // si era -1 se debe reestructurar el arbol
          case -1:
          /* reestructuración del árbol */
          nodo1 = nodo->izq;

          /* Rotacion II */
          if (nodo1->FE <= 0) {
            nodo->izq = nodo1->der;
            nodo1->der = nodo;
            nodo->FE = 0;
            nodo = nodo1;

          }else{
            /* Rotacion ID */
            nodo2 = nodo1->der;
            nodo->izq = nodo2->der;
            nodo2->der = nodo;
            nodo1->der = nodo2->izq;
            nodo2->izq = nodo1;

            // cambio dentro de los FE según la rotación si tiene elemento en la izquierda
            if (nodo2->FE == -1){
              nodo->FE = 1;
            }else{
              nodo->FE =0;
            }

            // cambio dentro de los FE según la rotación si tiene elemento en la derecho
            if(nodo2->FE == 1){
              nodo1->FE = -1;
            }else{
              nodo1->FE = 0;
            }
            nodo = nodo2;
          }
          nodo->FE = 0;
          *BO = FALSE;
          break;
        }
      }

    // si lo que se desea agregar no es menor al que se revisa
    }else{
      // si es mayor al nodo revisado retorna un entero positivo
      if (infor.compare(nodo->info) >= 1){
        // se inserta por la derecha
        InsercionBalanceado(nodo->der, BO, infor);

        // si el arbol a crecido
        if(*BO == TRUE) {
          // depende de su factor de equlibrio el balanceo que tendrá la inserción
          switch (nodo->FE) {
            // si era -1 será 0
            case -1:
            nodo->FE = 0;
            *BO = FALSE;
            break;
            // si era 0 será 1
            case 0:
            nodo->FE = 1;
            break;

            // si era 1 entonces se debe reestructurar
            case 1:
            /* reestructuración del árbol */
            nodo1 = nodo->der;

            if (nodo1->FE >= 0) {
              /* Rotacion DD */
              nodo->der = nodo1->izq;
              nodo1->izq = nodo;
              nodo->FE = 0;
              nodo = nodo1;

              }else{
              /* Rotacion DI */
              nodo2 = nodo1->izq;
              nodo->der = nodo2->izq;
              nodo2->izq = nodo;
              nodo1->izq = nodo2->der;
              nodo2->der = nodo1;

              if (nodo2->FE == 1){
                nodo->FE = -1;
              }else{
                nodo->FE = 0;
              }

              if (nodo2->FE == -1){
                nodo1->FE = 1;
              }else{
                nodo1->FE = 0;
              }
              nodo = nodo2;
            }
            nodo->FE = 0;
            *BO = FALSE;
            break;
          }
        }
        // si no es  mayor ni menor entonces es igual! en ese caso no se puede agregar
      }else{
        cout << "El nodo ya se encuentra en el árbol" << endl;
      }
    }
    // cuando el nodo no existe
  }else{
    // nuevo nodo
    nodo = new Nodo;
    nodo->izq = NULL;
    nodo->der = NULL;
    nodo->info = infor;
    nodo->FE = 0;
    *BO = TRUE;
  }
  // se reemplaza con el temporal
  nodo_rev = nodo;
}

// realiza la búsqueda de un nodo, existe por referencia que cambia si se encuentra o no
void Arbol::Busqueda(Nodo *nodo, string infor, bool &existe) {
  //si el nodo a revisar existe
  if (nodo != NULL) {
    // si el que se desea buscar es menor al que se está revisando
    if (infor.compare(nodo->info) <= -1){
      // se busca por la izquierda
      Busqueda(nodo->izq, infor, existe);
    }else{
      // si el que se desea buscar es mayor al que se está revisando
      if (infor.compare(nodo->info) >= 1){
        // se busca por la derecha
        Busqueda(nodo->der,infor, existe);
      }else{
        // si no es mayor ni menor entonces es igual
        cout << "El nodo SI se encuentra en el árbol" << endl;
        existe = true;
      }
    }
  } else {
    cout << "El nodo NO se encuentra en el árbol" << endl;
    existe = false;
  }
}

// tipo de reestructura a la izquierda cuando se debe eliminar
void Arbol::Restructura1(Nodo **nodocabeza, int *BO) {
  Nodo *nodo, *nodo1, *nodo2;
  nodo = *nodocabeza;

  if (*BO == TRUE) {
    switch (nodo->FE) {
      case -1:
      nodo->FE = 0;
      break;

      case 0:
      nodo->FE = 1;
      *BO = FALSE;
      break;

      case 1:
      // se procede a balancear el arbol dependiendo de su factor de equilibrio
      nodo1 = nodo->der;

      if (nodo1->FE >= 0) {
        /* rotacion DD */
        nodo->der = nodo1->izq;
        nodo1->izq = nodo;

        switch (nodo1->FE) {
          case 0:
          nodo->FE = 1;
          nodo1->FE = -1;
          *BO = FALSE;
          break;

          case 1:
          nodo->FE = 0;
          nodo1->FE = 0;
          *BO = FALSE;
          break;
        }
        nodo = nodo1;
      } else {
        /* Rotacion DI */
        nodo2 = nodo1->izq;
        nodo->der = nodo2->izq;
        nodo2->izq = nodo;
        nodo1->izq = nodo2->der;
        nodo2->der = nodo1;

        if (nodo2->FE == 1)
          nodo->FE = -1;
        else
          nodo->FE = 0;

        if (nodo2->FE == -1)
          nodo1->FE = 1;
        else
          nodo1->FE = 0;

        nodo = nodo2;
        nodo2->FE = 0;
        *BO = FALSE;
      }
      break;
    }
  }
  *nodocabeza = nodo;
}

// tipo de reestructura a la derecha cuando se debe eliminar
void Arbol::Restructura2(Nodo **nodocabeza, int *BO) {
  Nodo *nodo, *nodo1, *nodo2;
  nodo = *nodocabeza;

  if (*BO == TRUE) {
    switch (nodo->FE) {
      case 1:
      nodo->FE = 0;
      break;

      case 0:
      nodo->FE = -1;
      *BO = FALSE;
      break;

      case -1:
      // se procede a balancear el arbol dependiendo de su factor de equilibrio
      nodo1 = nodo->izq;
      if (nodo1->FE<=0) {
        /* rotacion II */
        nodo->izq = nodo1->der;
        nodo1->der = nodo;
        switch (nodo1->FE) {
          case 0:
          nodo->FE = -1;
          nodo1->FE = 1;
          *BO = FALSE;
          break;

          case -1:
          nodo->FE = 0;
          nodo1->FE = 0;
          *BO = FALSE;
          break;
          }

          nodo = nodo1;
        } else {
          /* Rotacion ID */
          nodo2 = nodo1->der;
          nodo->izq = nodo2->der;
          nodo2->der = nodo;
          nodo1->der = nodo2->izq;
          nodo2->izq = nodo1;

          if (nodo2->FE == -1)
            nodo->FE = 1;
          else
            nodo->FE = 0;

          if (nodo2->FE == 1)
            nodo1->FE = -1;
          else
            nodo1->FE = 0;

          nodo = nodo2;
          nodo2->FE = 0;
        }
        break;
    }
  }
  *nodocabeza = nodo;
}

// cambio para borrar
void Arbol::Borra(Nodo **aux1, Nodo **otro1, int *BO) {
  // temporales
  Nodo *aux, *otro;
  aux = *aux1;
  otro = *otro1;

  // si existe el de la derecha
  if (aux->der != NULL) {
    // se procede a seguir por ese lado
    Borra(&(aux->der), &otro, BO);
    Restructura2(&aux, BO);
    // si no existe por la derecha
  } else {
    // se elimina el info
    otro->info = aux->info;
    aux = aux->izq;
    *BO = TRUE;
  }
  // se igualan a los temporales
  *aux1 = aux;
  *otro1 = otro;
}

// procedimiento de eliminación
void Arbol::EliminacionBalanceado(Nodo **nodocabeza, int *BO, string infor) {
  // temporales
  Nodo *nodo, *otro;
  nodo = *nodocabeza;

  // si el nodo a revisar existe
  if (nodo != NULL) {
    // el que se desea eliminar es menor
    if (infor.compare(nodo->info) <= -1){
      // se procede a seguir buscando por la izquierda hasta encontrarlo
      EliminacionBalanceado(&(nodo->izq), BO, infor);
      Restructura1(&nodo, BO); //luego se reestrucura
    } else {
      // el que se desea eliminar es mayor
      if (infor.compare(nodo->info) >= 1){
        // se procede a seguir buscando por la derecha hasta encontrarlo
        EliminacionBalanceado(&(nodo->der), BO, infor);
        Restructura2(&nodo, BO); //luego se reestrucura
      } else {
        // se encuentra el nodo a eliminar
        otro = nodo; //temporal
        // no existe nodo por la derecha entonces se reemplaza por el de izquierda
        if (otro->der == NULL) {
          nodo = otro->izq;
          *BO = TRUE;
          // si existe un nodo a la derecha
        } else {
          // pero no por la izquierda, entonces se reemplaza el nodo por el de la derecha
          if (otro->izq==NULL) {
            nodo = otro->der;
            *BO = TRUE;
            // cuando tenga dos ramas existentes
          } else {
            *BO = FALSE;
            // se procede a borrar y reestructurar
            Borra(&(otro->izq), &otro, BO);
            Restructura1(&otro, BO);
            otro = NULL; // se elimina el temporal
          }
        }
      }
    }
  } else {
    cout << "El nodo NO se encuentra en el árbol" << endl;
  }
  *nodocabeza = nodo;
}

void Arbol::crear_grafo(Nodo *raiz){
  Grafo *g = new Grafo(raiz);
}
