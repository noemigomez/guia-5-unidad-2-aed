#include <iostream>
#include <unistd.h>
#include <fstream>
using namespace std;
#include <string>

//nodo
#include "programa.h"

// grafo
#include "Grafo.h"

/*crea el archivo .txt de salida y
  le agrega contenido del árbol para generar el grafo (imagen).*/


Grafo::Grafo(Nodo *nodo){
  ofstream fp;
  // se genera el txt
  fp.open("grafo.txt");
  fp << "digraph G {" << endl;
  fp << "node [style=filled fillcolor=pink];" << endl;

  fp << "nullraiz [shape=point];" << endl;
  fp << "nullraiz->" << "\"" << nodo->info << "\"" << " ";
  fp << "[label=" << nodo->FE << "];" << endl;
  // se llama a la funcion que recorre
  recorrer(nodo, fp);

  fp << "}";
  // se ciera el txt
  fp.close();
  // comando que genera la imagen a partir del txt
  system("dot -Tpng -ografo.png grafo.txt");
  // comando que abre la imagen
  system("eog grafo.png &");

}


//recorre en árbol en preorden y agrega datos al archivo.
void Grafo::recorrer(Nodo *p, ofstream &fp){
  if (p != NULL) {
   // se escribe el nodo y la rama izquierda
   if (p->izq != NULL) {
     fp << "\"" << p->info << "\"" << "->" << "\"" <<p->izq->info << "\"" << "[label=" <<p->izq->FE  << "];" << endl;
   } else{
     fp <<  "\"" << p->info << "i\"" << " [shape=point];" << endl;
     fp << "\"" <<p->info << "\"" << "->" << "\"" << p->info << "i\"" << ";" << endl;
   }
   // se escribe la rama derecha
   if (p->der != NULL) {
     fp << "\"" <<p->info << "\"" << "->" << "\"" << p->der->info << "\"" << "[label=" <<p->der->FE << "];" << endl;;
   } else{
     fp <<  "\"" << p->info << "d\""  << " [shape=point];" << endl;
     fp << "\"" << p->info << "\"" << "->" << "\"" << p->info << "d\"" << ";" << endl;
   }
   // se recorre el arbol por recursividas, primero izquierda despues derecha
   recorrer(p->izq, fp);
   recorrer(p->der, fp);
 }

}
