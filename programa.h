#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
using namespace std;

/* estructura del nodo */
typedef struct _Nodo {
  struct _Nodo *izq;
  struct _Nodo *der;
  string info;
  int FE;
} Nodo;
