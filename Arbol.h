#include <iostream>
#include <unistd.h>
#include <fstream>
using namespace std;
#include <string>

#ifndef ARBOL_H
#define ARBOL_H

class Arbol{
  private:

  public:

      //constructor
      Arbol();

      // inserta un elemento balanceado
      void InsercionBalanceado(Nodo *&nodo_rev, int *BO, string infor);
      // busca un nodo
      void Busqueda(Nodo *nodo, string infor, bool &existe);
      // reestrucutura hacia la izquierda
      void Restructura1(Nodo **nodocabeza, int *BO);
      // reestrucrura hacia la derecha
      void Restructura2(Nodo **nodocabeza, int *BO);
      // borra un nodo
      void Borra(Nodo **aux1, Nodo **otro1, int *BO);
      // busca y elimina un nodo
      void EliminacionBalanceado(Nodo **nodocabeza, int *BO , string infor);
      // crea un gráfico
      void crear_grafo(Nodo *raiz);
};
#endif
