#include <iostream>
#include <unistd.h>
#include <fstream>
#include <string>
using namespace std;

#include "programa.h"

#include "Arbol.h"

#include "Grafo.h"

#define TRUE 1
#define FALSE 0

// muestra menu
void Menu() {
  int opcion;
  // elemento y elemento2, cuando se necesite mas de uno en modificar (uno elimina y otro se inserta)
  string elemento, elemento2;

  bool se_puede_eliminar, se_puede_insertar, prueba;

  // indica si la altura del árbol creció
  int inicio;

  // se crea un árbol
  Arbol *avl = new Arbol();

  // se abre el archivo con pdbs
  ifstream archivo("pdbs.txt");
  string linea;

  system("clear"); //limpia la pantalla

  // se crea nodo raiz
  Nodo *raiz = NULL;

  // llena el arbol creado con los pdb del archivo
  while(getline(archivo, linea)){
    inicio = FALSE;
    // Se inserta cada id al arbol
    avl->InsercionBalanceado(raiz, &inicio, linea);
  }

  // se repite hasta que seleccione salir o una opcion no dicha (por ejemplo, 10)
  do{
    cout << "\n--------------------" << endl;
    cout << "1) Insertar ID" << endl;
    cout << "2) Buscar ID" << endl;
    cout << "3) Eliminar ID" << endl;
    cout << "4) Modificar ID" << endl;
    cout << "5) Generar grafo" << endl;
    cout << "0) Salir" << endl;
    cout << "Seleccione una opción: ";
    cin >> opcion;

    // segun lo que seleccione
    switch(opcion) {
      case 1:
      cout << "Ingresar elemento: ";
      cin >> elemento;
      inicio = FALSE;
      // se inserta elemento al arbol
      avl->InsercionBalanceado(raiz, &inicio, elemento);
      break;

      case 2:
      cout << "Buscar elemento: ";
      cin >> elemento;
      // se busca elemento
      avl->Busqueda(raiz, elemento, prueba);
      break;

      case 3:
      cout << "Eliminar elemento: ";
      cin >> elemento;
      inicio = FALSE;
      // se elimina elemento
      avl->EliminacionBalanceado(&raiz, &inicio, elemento);
      break;

      case 4:
      cout << "Ingrese elemento a eliminar: ";
      cin >> elemento;
      /*para poder modificar un elemento dentro del árbol el que se desea eliminar debe existir
        y por el que se desea reemplazar no puede estar repretido. Por lo tanto la función recibe por referencia
        un bool que cambia según si encuentra el nodo dentro del árbol. En el caso de eliminar debe retornar un true
        mientras que en caso del que se debe insertar debe ser un false, ya que no debe estar*/
      avl->Busqueda(raiz, elemento, se_puede_eliminar);
      // el nodo a eliminar está en el árbol
      if(se_puede_eliminar){
        cout << "Ingrese elemento para reemplazar: ";
        cin >> elemento2;
        avl->Busqueda(raiz, elemento2, se_puede_insertar);
        // el nodo a insertar no está dentro del árbol
        if(se_puede_insertar == false){
          avl->EliminacionBalanceado(&raiz, &inicio, elemento);
          avl->InsercionBalanceado(raiz, &inicio, elemento2);
          cout << "Se ha modificado el nodo " << elemento << " por el nodo " << elemento2 << endl;
        }else{
          cout << "No se puede modificar debido a que el ID que desea reemplazar no existe" << endl;
        }
      }else{
        cout << "No se puede modificar debido a que el ID que desea eliminar no existe." << endl;
      }
      break;

      case 5:
      avl->crear_grafo(raiz);
      break;

      case 0:
      exit(1);
      break;
    }

  } while (opcion<6 && opcion>0);
}

// función principal
int main(int argc, char **argv) {

  // función que actua como menú
  Menu();

  return 0;
}
